require "custom_erb_config/version"

module CustomErbConfig
    def self.load_config_yml(name)
        # TODO allow changing this path
        config_path = Rails.root.join('config', 'custom', "#{name}.yml")

        if File.exist?(config_path)
            config_file = File.read(config_path)
            config_erb = ERB.new(config_file).result

            return YAML.load(config_erb)[Rails.env]
        end

        return {}
    end

    def self.erb_config(name)
        config_yml = load_config_yml(name)
        # TODO should this only load on local?
        config_yml.merge!(load_config_yml("#{name}.override")) if Rails.env.local?
        Rails.application.config.send("#{name}=".to_sym, config_yml)
    end
end
