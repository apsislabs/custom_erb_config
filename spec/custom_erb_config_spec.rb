require 'spec_helper'

describe CustomErbConfig do
    it 'has a version number' do
        expect(CustomErbConfig::VERSION).not_to be nil
    end

    describe 'load_config_yml' do
        # TODO fix and enable TESTs
        xit 'loads an orginal yml' do
            yml_result = CustomErbConfig.load_config_yml('sample')
            expect(yml_result['sample_1']).to eq(1)
            expect(yml_result['nested']['sample_2']).to eq()
        end

        xit 'loads an override yml' do
            yml_result = CustomErbConfig.load_config_yml('sample')
            expect(yml_result['sample_1']).to eq(1)
            expect(yml_result['nested']['sample_2']).to eq()
        end
    end

    describe 'erb_config' do
        xit 'loads merged override' do
            CustomErbConfig::erb_config('sample')
            config_yml = Rails.application.config.sample
            expect(config_yml['sample_1']).to eq('Overridden_1')
            expect(config_yml['nested']['sample_2']).to eq('Overridden_2')
            expect(config_yml['sample_3']).to eq(3)
        end
    end
end
